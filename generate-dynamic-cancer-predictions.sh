if [ $# -lt 7 ]; then
	echo Error: Wrong number of arguments
	echo Usage: $0 [detection_pv_threshold] [majority_call] [correlation_type: P - Pearson, S - Spearman] [rand_run] [min_ages_to_be_expressed] [pv_threshold] [output_dir]
	echo Optional parameters: -p [no_of_parallel_processes] -start [start_step] -end [end_step]
	echo Example: $0 0.04 0.5 P 999999 6 0.1 sample-output -p 40
	echo Where sample-output contains a folder called 'intermediate-files/dynamic-ppis' that contains the dynamic-ppis
	exit
fi

#network=$1
#shift
#gdsAgeIntegratedDir=$1
#shift
dpThreshold=$1
shift
majorityCall=$1
shift
correlationType=$1
shift
randRun=$1
shift
minSample=$1
shift
pThreshold=$1
shift
outputDir=$1
shift

if [ "$correlationType" == "P" ]; then
	correlationType=1 # 1 - Pearson
elif [ "$correlationType" == "S" ]; then
	correlationType=2 # 2 - Spearman
else
	echo Error: Wrong [correlation-type: P - Pearson, S - Spearman]
	exit
fi

val=`expr $# % 2`
if [ $val -ne 0 ]; then
	echo Error: Optional parameters are misconfigured
	echo Optional parameters: -p [no-of-parallel-processes] -start [start-step] -end [end-step]
	exit
fi

start=1
end=13 #CHANGED THIS!
maxScreen=1

noOptParameters=`expr $# / 2`
for((i=1;i<=$noOptParameters;i++)); do
	option=$1
	shift
	val=$1
	shift
	if [ "$option" == "-start" ]; then
		start=$val
	elif [ "$option" == "-end" ]; then
		end=$val
	elif [ "$option" == "-p" ]; then
		maxScreen=$val
	else
		echo Error: Optional parameters are misconfigured
		echo Optional parameters: -p [no-of-parallel-processes] -start [start-step] -end [end-step]
		exit
	fi
done

intermediateDir="$outputDir/intermediate-files"

if [ -d $outputDir ]; then
	echo Directory $outputDir already exists.
	echo Make your choice to proceed: D - Delete previous results and restart, C - Continue with current results, T - Terminate
	read choice
	if [ "$choice" == "D" -o "$choice" == "d" ]; then
		rm -r $outputDir
		mkdir $outputDir
		mkdir $intermediateDir
	elif [ "$choice" == "C" -o "$choice" == "c" ]; then
		echo Program resumes.
	elif [ "$choice" == "T" -o "$choice" == "n" ]; then
		echo Program terminated without computations.
		exit
	else
		echo Error: Wrong option selected.
		echo Program terminated without computations.
		exit
	fi
else
	mkdir $outputDir
	mkdir $intermediateDir
fi


dynamicExprDir="$intermediateDir/dynamic-expression"
dynamicPPIDir="$intermediateDir/dynamic-ppis"
dynamicLedaDir="$intermediateDir/dynamic-leda"

ndump2Dir="$intermediateDir/ndump2dir"
dynamicPPIOptDir="$intermediateDir/dynamic-ppis-opt"
shortestPathDir="$intermediateDir/shortestpathdir"

#betwcDir="$intermediateDir/betwcdir"
closecDir="$intermediateDir/closecdir"
cluscDir="$intermediateDir/cluscdir"
degcDir="$intermediateDir/degcdir"
eccDir="$intermediateDir/eccdir"
gdcDir="$intermediateDir/gdcdir"
kcDir="$intermediateDir/kcdir"

corrShortDir="$intermediateDir/corrshort"
corrShortAdjDir="$intermediateDir/corrshortadj"
corrLongDir="$intermediateDir/corrlong"
ageGeneDir="$intermediateDir/agegene"
ageGeneAdjDir="$outputDir/aging-predictions"

allSampleFile="$intermediateDir/all-sample.txt"

#10/30/17: removed getting file names from gdsAgeIntegratedDir here (moved getting filenames to part 3.5)


# program starts here

#module load R

# parts 1 and 2 removed:
#	-Were originally used to compute dynamic-ppis using the network and gene expression directory.
#	-To use them again, use the generate-predictions.sh script.

# part 3
if [ $start -le 1 -a $end -ge 1 ]; then
	if [ ! -d $dynamicPPIDir ]; then
		echo Directory $dynamicPPIDir not found
		exit
	fi

	if [ -d $dynamicLedaDir ]; then
		rm -r $dynamicLedaDir
	fi
	mkdir $dynamicLedaDir

	for inputFile in `ls $dynamicPPIDir`; do
		outputFile=${inputFile%\.*}
		./bin/list2leda $dynamicPPIDir/$inputFile > templeda.gw
		./bin/sanitize templeda.gw > $dynamicLedaDir/$outputFile.gw
		rm templeda.gw
		echo 1 dynamic leda: $outputFile is done
	done
	echo 1 dynamic leda computed
fi

													
# part 3.5: getting names of files (formerly from the sample-ge-data filenames)
totalSample=0
echo -n "" > $allSampleFile
for inputFile in `ls $dynamicPPIDir`; do
	outputFile=${inputFile%\.*}
	echo $outputFile >> $allSampleFile
	totalSample=`expr $totalSample + 1`
done
													

# part 4
if [ $start -le 2 -a $end -ge 2 ]; then
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $ndump2Dir ]; then
		rm -r $ndump2Dir
	fi
	mkdir $ndump2Dir

	for inputFile in `ls $dynamicLedaDir`; do
		outputFile=${inputFile%\.*}
		./bin/ncount $dynamicLedaDir/$inputFile $ndump2Dir/$outputFile
		rm $ndump2Dir/$outputFile
		rm $ndump2Dir/$outputFile.cl*
		rm $ndump2Dir/$outputFile.gr_freq
		echo 2 ndump2: $outputFile is done
	done
	echo 2 ndump2 computed
fi

# part 5
if [ $start -le 3 -a $end -ge 3 ]; then
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $shortestPathDir ]; then
		rm -r $shortestPathDir
	fi
	mkdir $shortestPathDir

	for inputFile in `ls $dynamicLedaDir`; do
		outputFile=${inputFile%\.*}
		./bin/sh_path $dynamicLedaDir/$inputFile $shortestPathDir/$outputFile.shpath
		echo 3 shortest path: $outputFile is done
	done
	echo 3 shortest path computed
fi

# part 6
if [ $start -le 4 -a $end -ge 4 ]; then
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $dynamicPPIOptDir ]; then
		rm -r $dynamicPPIOptDir
	fi
	mkdir $dynamicPPIOptDir

	for inputFile in `ls $dynamicLedaDir`; do
		outputFile=${inputFile%\.*}
		./bin/leda2ppi $dynamicLedaDir/$inputFile $dynamicPPIOptDir/$outputFile.ppi
		echo 4 dynamic ppi optimized: $outputFile is done
	done
	echo 4 dynamic ppi optimized computed
fi

# part 7 - betwc
echo betwc skipped successfully
# skipped successfully

# part 8
if [ $start -le 5 -a $end -ge 5 ]; then
	if [ ! -d $shortestPathDir ]; then
		echo Directory $shortestPathDir not found
		exit
	fi
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $closecDir ]; then
		rm -r $closecDir
	fi
	mkdir $closecDir

	for inputFile in `ls $shortestPathDir`; do
		outputFile=${inputFile%\.*}
		./bin/closec-and-ecc $shortestPathDir/$inputFile $closecDir/$outputFile.closec 1
		./bin/zero-degree $dynamicLedaDir/$outputFile.gw >> $closecDir/$outputFile.closec
		echo 5 closec: $outputFile is done
	done
	echo 5 closec computed
fi

# part 9
if [ $start -le 6 -a $end -ge 6 ]; then
	if [ ! -d $ndump2Dir ]; then
		echo Directory $ndump2Dir not found
		exit
	fi

	if [ -d $cluscDir ]; then
		rm -r $cluscDir
	fi
	mkdir $cluscDir

	for inputFile in `ls $ndump2Dir`; do
		outputFile=${inputFile%\.*}
		cut -d ' ' -f1,2,5 $ndump2Dir/$inputFile > $intermediateDir/clusc.tmp
		./bin/clust-coeff $intermediateDir/clusc.tmp $cluscDir/$outputFile.clusc
		rm $intermediateDir/clusc.tmp
		echo 6 clusc: $outputFile is done
	done
	echo 6 clusc computed
fi

# part 10
if [ $start -le 7 -a $end -ge 7 ]; then
	if [ ! -d $ndump2Dir ]; then
		echo Directory $ndump2Dir not found
		exit
	fi

	if [ -d $degcDir ]; then
		rm -r $degcDir
	fi
	mkdir $degcDir

	for inputFile in `ls $ndump2Dir`; do
		outputFile=${inputFile%\.*}
		cut -d ' ' -f1,2 $ndump2Dir/$inputFile > $degcDir/$outputFile.degc
		echo 7 degc: $outputFile is done
	done
	echo 7 degc computed
fi

# part 11
if [ $start -le 8 -a $end -ge 8 ]; then
	if [ ! -d $shortestPathDir ]; then
		echo Directory $shortestPathDir not found
		exit
	fi
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $eccDir ]; then
		rm -r $eccDir
	fi
	mkdir $eccDir

	for inputFile in `ls $shortestPathDir`; do
		outputFile=${inputFile%\.*}
		./bin/closec-and-ecc $shortestPathDir/$inputFile $eccDir/$outputFile.ecc 2
		./bin/zero-degree $dynamicLedaDir/$outputFile.gw >> $eccDir/$outputFile.ecc
		echo 8 ecc: $outputFile is done
	done
	echo 8 ecc computed
fi

# part 12
if [ $start -le 9 -a $end -ge 9 ]; then
	if [ ! -d $ndump2Dir ]; then
		echo Directory $ndump2Dir not found
		exit
	fi

	if [ -d $gdcDir ]; then
		rm -r $gdcDir
	fi
	mkdir $gdcDir

	for inputFile in `ls $ndump2Dir`; do
		outputFile=${inputFile%\.*}
		./bin/gdc $ndump2Dir/$inputFile $gdcDir/$outputFile.gdc
		echo 9 gdc: $outputFile is done
	done
	echo 9 gdc computed
fi

# part 13
if [ $start -le 10 -a $end -ge 10 ]; then
	if [ ! -d $dynamicPPIOptDir ]; then
		echo Directory $dynamicPPIOctDir not found
		exit
	fi
	if [ ! -d $dynamicLedaDir ]; then
		echo Directory $dynamicLedaDir not found
		exit
	fi

	if [ -d $kcDir ]; then
		rm -r $kcDir
	fi
	mkdir $kcDir

	for inputFile in `ls $dynamicPPIOptDir`; do
		outputFile=${inputFile%\.*}
		./bin/kCoreness $dynamicPPIOptDir/$inputFile $kcDir/$outputFile.kc
		./bin/zero-degree $dynamicLedaDir/$outputFile.gw >> $kcDir/$outputFile.kc
		echo 10 kc: $outputFile is done
	done
	echo 10 kc computed
fi

# part 14
if [ $start -le 11 -a $end -ge 11 ]; then
#	if [ ! -d $betwcDir ]; then
#		echo Directory $betwcDir not found
#		exit
#	fi
	if [ ! -d $closecDir ]; then
		echo Directory $closecDir not found
		exit
	fi
	if [ ! -d $cluscDir ]; then
		echo Directory $cluscDir not found
		exit
	fi
	if [ ! -d $degcDir ]; then
		echo Directory $degcDir not found
		exit
	fi
	if [ ! -d $eccDir ]; then
		echo Directory $eccDir not found
		exit
	fi
	if [ ! -d $gdcDir ]; then
		echo Directory $gdcDir not found
		exit
	fi
	if [ ! -d $kcDir ]; then
		echo Directory $kcDir not found
		exit
	fi

	if [ -d $corrShortDir ]; then
		rm -r $corrShortDir
	fi
	mkdir $corrShortDir
	if [ -d $corrLongDir ]; then
		rm -r $corrLongDir
	fi
	mkdir $corrLongDir

	#corrDirName[1]=$betwcDir
	corrDirName[1]=$closecDir
	corrDirName[2]=$cluscDir
	corrDirName[3]=$degcDir
	corrDirName[4]=$eccDir
	corrDirName[5]=$gdcDir
	corrDirName[6]=$kcDir

	#cent[1]="betwc"
	cent[1]="closec"
	cent[2]="clusc"
	cent[3]="degc"
	cent[4]="ecc"
	cent[5]="gdc"
	cent[6]="kc"

	./bin/get-age $allSampleFile $intermediateDir/ages.tmp
	 ./bin/random-reshuffle $intermediateDir/ages.tmp $randRun $intermediateDir/precomputedvals.tmp

	for((i=1;i<=6;i++)); do #changed 7 to 6
		if [ $maxScreen -gt 1 ]; then
			while true; do
				cnt=$(screen -ls | grep Detached | wc -l)
				if [ $cnt -lt $maxScreen ]; then
					break;
				fi
				sleep 5
			done

			screen -m -d sh script/corr-age-cent.sh $allSampleFile $intermediateDir/ages.tmp $intermediateDir/precomputedvals.tmp ${corrDirName[$i]} ${cent[$i]} $correlationType $corrShortDir $corrLongDir $intermediateDir
		else
			sh script/corr-age-cent.sh $allSampleFile $intermediateDir/ages.tmp $intermediateDir/precomputedvals.tmp ${corrDirName[$i]} ${cent[$i]} $correlationType $corrShortDir $corrLongDir $intermediateDir
		fi
		echo 14 corr-age-cent: ${cent[$i]} is done
	done

	if [ $maxScreen -gt 1 ]; then
		while true; do
			cnt=0
			for file in `ls $corrShortDir`; do
				v=$(tail -1 $corrShortDir/$file | awk '{ print NF }')
				cnt=`expr $cnt + $v`
			done
			if [ $cnt -eq 28 ]; then
				break;
			fi
			sleep 5
			echo 14 corr-age-cent is computing
		done
	fi
	rm $intermediateDir/ages.tmp
	rm $intermediateDir/precomputedvals.tmp

	echo 14 corr-age-cent computed
fi

# part 15
if [ $start -le 12 -a $end -ge 12 ]; then
	if [ ! -d $corrShortDir ]; then
		echo Directory $corrShortDir not found
		exit
	fi
	if [ -d $corrShortAdjDir ]; then
		rm -r $corrShortAdjDir
	fi
	mkdir $corrShortAdjDir

	for inputFile in `ls $corrShortDir`; do
		./bin/filter-min-sample $corrShortDir/$inputFile $corrShortDir$inputFile-minsample $minSample
		cut $corrShortDir$inputFile-minsample -f1 > $corrShortDir$inputFile-geneid
		cut $corrShortDir$inputFile-minsample -f2 > $corrShortDir$inputFile-corr
		cut $corrShortDir$inputFile-minsample -f3 > $corrShortDir$inputFile-nosample
		cut $corrShortDir$inputFile-minsample -f4 > $corrShortDir$inputFile-pv
		Rscript script/fdr.r $corrShortDir$inputFile-pv $corrShortDir$inputFile-fdr
		paste $corrShortDir$inputFile-geneid $corrShortDir$inputFile-corr $corrShortDir$inputFile-nosample $corrShortDir$inputFile-fdr > $corrShortAdjDir/$inputFile
		rm $corrShortDir$inputFile-geneid
		rm $corrShortDir$inputFile-corr
		rm $corrShortDir$inputFile-nosample
		rm $corrShortDir$inputFile-pv
		rm $corrShortDir$inputFile-fdr
		rm $corrShortDir$inputFile-minsample
	done

	echo 12 adjusted pv computed
fi

# part 16
if [ $start -le 13 -a $end -ge 13 ]; then
	if [ ! -d $corrShortAdjDir ]; then
		echo Directory $corrShortDir not found
		exit
	fi

	if [ -d $ageGeneAdjDir ]; then
		rm -r $ageGeneAdjDir
	fi
	mkdir $ageGeneAdjDir
	
	i=0
	for inputFile in `ls $corrShortAdjDir`; do
		i=`expr $i + 1`
		cent=${inputFile##*-}
		./bin/high-corr-age-genes $corrShortAdjDir/$inputFile $ageGeneAdjDir/predictions-by-$cent.txt $pThreshold $minSample > /dev/null
	done
	sh script/union.sh $ageGeneAdjDir $ageGeneAdjDir/predictions-by-any.txt > /dev/null
	echo 13 aging predictions computed
fi

rm $allSampleFile
